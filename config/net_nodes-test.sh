me "$BASH_SOURCE" #tradition

# Not used by javascript SDK, but we will want to be able to query particular nodes in the future.

nodes['0.0.3']="0.testnet.hedera.com:50211"
nodes['0.0.4']="1.testnet.hedera.com:50211"
nodes['0.0.5']="2.testnet.hedera.com:50211"
nodes['0.0.6']="3.testnet.hedera.com:50211"
 
nodes['favourite']="${nodes['0.0.3']}"

