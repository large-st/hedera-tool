# This file is the top level config file for hedera-tool
# providing configuration of the configuration!
#
# a) the names and potential locations of config files
# b) where to find commands and topics

# Traditionally every file in the groan framework reports
# to the debug log, and defined variables my_name my_dir
me "$BASH_SOURCE" 

# pre-release-title of this command suite
c_description="Hedera - Hello Future"

# Since the groan framework is heirarchically composable
# this tool may be re-used in other contexts.

# The name of this tool context
g_context="hedera-tool"

# In an attempt to smooth out platform ugliness groan sets 
# the variable $g_PLAFTFORM using ${BASH_VERSINFO[5]}
# Example values include:
# High-Sierra = x86_64-apple-darwin17.7.0
# NixOS 20.03 = x86_64-unknown-linux-gnu

case "$g_PLATFORM" in
	*apple* )
		HB_GLOBAL_CONFIG_DIR="/Library/Preferences/${g_name}"
	;;
	*msys* )
	    HB_GLOBAL_CONFIG_DIR="/AppData/${g_name}" # (not attempted yet)
	;;
	*linux* )
	    HB_GLOBAL_CONFIG_DIR="/etc/${g_name}"
	;;
esac

# Locations searched for configuration files
# - add a genuine "global" location for 
#   the machine we are installed upon

g_config_options=("local" "config" "user" "global" "fallback")
g_configs=(
    "$(pwd)"  	                  # --local
    "$(pwd)/config"  	          # --config
    "$HOME/.config/$g_context"    # --user
    "$HB_GLOBAL_CONFIG_DIR"       # --global
    "${my_dir}"  	              # --fallback
)

function g_readConfig ()
{
  g_readConfig_

  [[ -z ${h_network+x} ]] && echo "The hashgraph network has not been selected in ${CONFIG}.conf"
  
  g_find_ "Nodes" "net_nodes-${h_network}.sh" "${g_configs[@]}" && source "${g_found}" || echo "Network node list not found"
  
  return 0
}

# If using python we use the shared library via PYTHONPATH
# PYTHONPATH="$PYTHONPATH:${g_dir}/hedera-sdk-python/target/debug"

# The location of the interface onto the sdk that we are using.
h_shim="$g_dir/shim-js"

# The location of common functions that we are using
h_lib="$g_dir/commands/lib"

# The number of tiny bars in an hbar
h_onebar=100000000
h_match_id='[0-9]+\.[0-9]+\.[0-9]+'

declare -Ag file_paths file_ids nodes
