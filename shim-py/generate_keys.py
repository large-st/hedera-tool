#!/usr/bin/env python3

import sys
import hedera

network = str(sys.argv[1])
name = str(sys.argv[2])

secret, mnemonic = hedera.SecretKey.generate("")
public = secret.public

print(f"{network}_{name}_secretWords='{mnemonic}'") 
print(f"{network}_{name}_secretKey='{secret}'")
print(f"{network}_{name}_publicKey='{public}'")
