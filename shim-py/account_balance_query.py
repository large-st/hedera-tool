#!/usr/bin/env python3

import sys
import hedera

network = str(sys.argv[1])
node = str(sys.argv[2])
operator = hedera.AccountId( sys.argv[3] ) 
operator_secret = hedera.SecretKey( sys.argv[4] )
 
print( f"operator {operator}" )

client = hedera.Client(node)

print( f"client {client}" )

#client.operator( operatorId )

balance = client.account(operator).balance().get()

print(f"balance = {balance} tinybars")
print(f"balance = {balance / 100000000.0} hbars")

# todo: info = client.account(hedera.AccountId("0:0:2")).info().get()
