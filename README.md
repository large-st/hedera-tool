# Hedera Tool

[Documentation WIki](https://gitlab.com/large-st/hedera-tool/-/wikis/home)

Bash implementation of the hedera SDK

# Design Decisions

In this context the interface to hedera is via nodejs and the hedera-js-sdk.

These scripts take all parameters via positional arguments so that they are
easily converted into simple python/rust functions that can be reused in 
python code or other contexts. It makes the interface clearer.

# Groan Framework

Within the groan framework, all of the bash sub-commands can be re-implemented in
pure python, which would also be able to use the functions as writtem.

http://gitlab.com/keithy/groan