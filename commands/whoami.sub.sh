# hedera whoami.sub.sh
#
# copywrite Keithy 2020
#
me "$BASH_SOURCE" #tradition

command="whoami"
description="who am I in the hedera universe"

options=\
"
$breadcrumbs <name>
--quiet | -Q   just print the value reqested
--id           print account id
--public       print account public key
--secret       print account private key/words
"

usage=\
"
$breadcrumbs                    print the id, (default: me) 
$breadcrumbs me
$breadcrumbs me --id --quiet    print only the id
"

$METADATAONLY && return

function print_whoami()
{
  local id_ref="${h_network}_${1}_accountId"
  local public_ref="${h_network}_${1}_publicKey"
  local secret_ref="${h_network}_${1}_secretKey"
  local words_ref="${h_network}_${1}_secretWords"

  [[ -z "${!id_ref+x}" ]] && echo "User '$1' not known" && return

  $PRINT_ACCOUNT_ID && print_kv "accountId" "${!id_ref}" "$1" 
  $PRINT_PUBLIC && print_kv "publicKey" "${!public_ref}"
  $PRINT_PRIVATE && print_kv "secretKey" "${!secret_ref}"
  $PRINT_PRIVATE && [[ -n "${!words_ref:-}" ]] && print_kv "secretWords" "${!words_ref}"
  
  return 0
}

g_declare_options PRINT_ACCOUNT_ID PRINT_PUBLIC PRINT_PRIVATE #false

PRINT_ACCOUNT_ID=$LOUD
PRINT_PUBLIC=$VERBOSE

list=()
for arg in "$@"
do
    case "$arg" in
      --id)
        PRINT_ACCOUNT_ID=true
      ;;
      --public)
        PRINT_PUBLIC=true
      ;;
      --secret | --secrets)
        PRINT_PRIVATE=true
      ;;
      --all)
        PRINT_ACCOUNT_ID=true
        PRINT_PUBLIC=true
        PRINT_PRIVATE=true
      ;;
      -*)
      :
      ;;
      *)
        list+=( "$arg" )
      ;;
    esac
done     
 
for each in "${list[@]:-$h_whoami}"
do
  print_whoami "$each"
done

exit 0

# License TBD