

# The node id that we query can be provided via an environment variable
function h_node_set()
{
    h_node="${nodes[${1:-favourite}]}"
}

function h_operator_set()
{
   op_name="${1:-$2}"
   
   local op_id_var="${h_network}_${op_name}_accountId"
   local op_secretKey_var="${h_network}_${op_name}_secretKey"
   
   op_id="${!op_id_var}"
   op_secretKey="${!op_secretKey_var}"
}
