# hedera whoami.sub.sh
#
# copywrite Keithy 2020
#
me "$BASH_SOURCE" #tradition

command="key"
description="Get account balances"

options=\
"
$breadcrumbs <name|id 1> [ <name|id 2> ] ...
--quiet | -Q          just print the value reqested
--operator=<name>     (default: ${h_whoami})
"

usage=\
"
$breadcrumbs                  print account balance, (default: ${h_whoami}) 
$breadcrumbs -Q               print only the account balance 
"

$METADATAONLY && return

g_declare_options GET_BALANCE GET_DIFF #false

GET_BALANCE=true #default (for now)
GET_DIFF=true

list=()
for arg in "$@"
do
    case "$arg" in
      --node=*)
        HB_NODE="${arg#--node=}"
      ;;
      --op=* | --operator=* )
        HB_OPERATOR="${arg#--op*=}"
      ;;
      --diff )
        GET_DIFF=true
      ;;
      -*)
        :
      ;;
      *)
        list+=( "$arg" )
      ;;
    esac
done

source "${h_lib}/common_functions.sh"

h_node_set "${HB_NODE:-}"
h_operator_set "${HB_OPERATOR:-}" "${h_whoami}"

for name in "${list[@]:-${op_name}}"
do
  if [[ "$name" =~ "$h_match_id" ]]; then
      query_id="$name"
  else
      query_id_var="${h_network}_${name}_accountId"
      query_id="${!query_id_var}"
      balance_var="${h_network}_${name}_balance"
      balance_prev="${!balance_var:-}"
      balance_prev=$(cat "/tmp/hb_${balance_var}" 2> /dev/null || echo 0)
  fi


  if $GET_BALANCE; then
      
      $DEBUG && echo "${h_shim}/account_balance_query.js" "${h_network}" "${h_node}" "${op_id}" "${op_secretKey}" "${query_id}"
      $DEBUG && "${h_shim}/account_balance_query.js" "${h_network}" "${h_node}" "${op_id}" "${op_secretKey}" "${query_id}"
      
      local balance=$("$h_shim/account_balance_query.js" "${h_network}" "${h_node}" "${op_id}" "${op_secretKey}" "${query_id}" 2> /tmp/hb_file_err || echo "")

      [[ -z "${balance}" ]] && validMsg="Not Available" || validMsg=""

      $LOUD && print_kv "account_id" "${query_id}" 
      
      print_kv "balance" "${balance}" "${validMsg}";
      balance_tbars="${balance/./}"

      if $GET_DIFF && [[ -n ${balance_prev+x} ]]; then
        (( balance_diff = balance_prev - balance_tbars ))
        print_kv "change" "${balance_diff}" "tinybars"
      fi

      if [[ -n ${balance_var+x} ]]; then
        echo "${balance_tbars}" > "/tmp/hb_${balance_var}"
      fi

  fi
done

exit 0

# License TBD
