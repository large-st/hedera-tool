# hedera pay.sub.sh
#
# copywrite Keithy 2020
#
# to do add confirmation

me "$BASH_SOURCE" #tradition

command="pay"
description="crypto transfer"

options=\
"
$breadcrumbs [ --options ... ] [ '<memo' ]
--operator=<name>     sender (default: ${h_whoami})
--to=<name>           named recipient
--recipient=\"0.0.x\"   recipient id
--hbar=<hbar>         amount in hbar
--tbar=<tbar>         amount in tinybar
--confirm | -Y        naturally
"

usage=\
"
$breadcrumbs --hbar=1 --to=friend 'Hello Future'
$breadcrumbs --tbar=1 --recipient="0.0.3" 'Hello Future' -V
"

$METADATAONLY && return

g_declare_options CRYPTO_TRANSFER EXPORT_RESULT #false

memo=""
for arg in "$@"
do
    case "$arg" in
      --node=*)
        HB_NODE="${arg#--node=}"
      ;;
      --op=* | --operator=* )
        HB_OPERATOR="${arg#--op*=}"
      ;;
      --sender=* )
        HB_OPERATOR="${arg#--sender=}"
      ;;
      --to=* )
        recipient_name="${arg#--to=}"
      ;;
      --recipient=* )
        recipient_id="${arg#--recipient=}"
      ;;
      --hbar=* )
        hbar="${arg#--hbar=}"
        (( tbar = hbar * h_onebar ))
        CRYPTO_TRANSFER=true
      ;;
      --tbar=* )
        tbar="${arg#--tbar=}"
        CRYPTO_TRANSFER=true
      ;;
      -*)
        :
      ;;
      *)
        memo="$arg"
      ;;
    esac
done

source "${h_lib}/common_functions.sh"

h_node_set "${HB_NODE:-}"
h_operator_set "${HB_OPERATOR:-}" "${h_whoami}"

if [[ -n ${recipient_name+x} ]]; then
    recipient_id_var="${h_network}_${recipient_name}_accountId"
    recipient_id="${!query_id_var}"
fi

[[ -z ${tbar+x} ]] && { echo "no amount specified"; exit; }

$CONFIRM || { echo "(append --confirm to send payment of ${tbar} tinybar from ${op_id} to ${recipient_id} memo:'${memo}' via ${h_network}net)" ; exit; }

if $CRYPTO_TRANSFER; then
    result=$("${h_shim}/crypto_transfer.js" "${h_network}" "${h_node}" "${op_id}" "${op_secretKey}" "${recipient_id}" "${tbar}" "${memo}" "receipt")
    
    $VERBOSE && echo "$result"
    source <(echo "$result")

    echo "$receipt"
fi

exit 0

# License TBD