# hedera whoami.sub.sh
#
# copywrite Keithy 2020
#
me "$BASH_SOURCE" #tradition

command="key"
description="generate keys"

options=\
"
$breadcrumbs <name>
--quiet | -Q   just print the value reqested
--id           print account id
--public       print account public key
--private      print account private key
"

usage=\
"
$breadcrumbs                    print the id, (default: me) 
$breadcrumbs me
$breadcrumbs me --id --quiet    print only the id
"

$METADATAONLY && return

g_declare_options GENERATE #false

GENERATE=true #default (for now)

list=()
for arg in "$@"
do
    case "$arg" in
      --new | --generate | --gen)
        GENERATE=true
      ;;
      -*)
        :
      ;;
      *)
        list+=( "$arg" )
      ;;
    esac
done     
 
for name in "${list[@]:-tmp}"
do
    if $GENERATE; then
      "${h_shim}/generate_keys.js" "${h_network}" "${name}"
      echo   
    fi
done

exit 0

# License TBD