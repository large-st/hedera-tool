# hedera info.sub.sh
#
# copywrite Keithy 2020
#
# to do add confirmation

me "$BASH_SOURCE" #tradition

command="info"
description="Account information query"

options=\
"
$breadcrumbs [ --options ... ] [ account-name | id ] ... 
--operator=<name>     who's asking (default: ${h_whoami})
--confirm | -Y        naturally
"

usage=\
"
$breadcrumbs "0.0.3" "0.0.4" me
$breadcrumbs me bob alice
"

$METADATAONLY && return

g_declare_options GET_INFO #false

GET_INFO=true

list=()
for arg in "$@"
do
    case "$arg" in
      --node=*)
        HB_NODE="${arg#--node=}"
      ;;
      --op=* | --operator=* )
        HB_OPERATOR="${arg#--op*=}"
      ;;
      -*)
        :
      ;;
      *)
        list+=( "$arg" )
      ;;
    esac
done

source "${h_lib}/common_functions.sh"

h_node_set "${HB_NODE:-}"
h_operator_set "${HB_OPERATOR:-}" "${h_whoami}"

$CONFIRM || { echo "(append --confirm to query account info for ("${list[*]:-${op_name}}") via ${h_network}net)" ; exit; }

for name in "${list[@]:-${op_name}}"
do
  if [[ "$name" == *.*.* ]]; then
      query_id="$name"
  else
      query_id_var="${h_network}_${name}_accountId"
      query_id="${!query_id_var}"
  fi

  if $GET_INFO; then
      
    result=$("${h_shim}/account_info_query.js" "${h_network}" "${h_node}" "${op_id}" "${op_secretKey}" "${query_id}" "info")
    
    $VERBOSE && echo "$result"
    source <(echo "$result")

    [[ "${info_isDeleted}" == "true" ]] && deletedMsg="Deleted" || deletedMsg="" 
    print_kv "id" "${info_accountId_shard}.${info_accountId_realm}.${info_accountId_account}" "$deletedMsg"
    [[ "$info_proxyAccountId_account" != "0" ]] && \
      print_kv "proxy" "${info_proxyAccountId_shard}.${info_proxyAccountId_realm}.${info_proxyAccountId_account}"
    [[ "$$info_isReceiverSignatureRequired" == "true" ]] && \
     print_kv "isReceiverSignatureRequired" "$info_isReceiverSignatureRequired"
    print_kv "renew" "$info_autoRenewPeriod"

  fi

done

exit 0

# License TBD