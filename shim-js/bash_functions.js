#!/usr/bin/env node

module.exports = {
  
  sourcify: function ( object, prefix = '' ) {
    if (object === Object(object)) {
      for (const property in object) {
        this.sourcify( object[property], prefix + '_' + property)
      }
    } else {
    value=String(object).replace(/"/g, '\\\"');
    console.log(`${prefix}="${value}"`);
    }  
  }

};
 