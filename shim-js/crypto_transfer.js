#!/usr/bin/env node

const bash = require('./bash_functions.js');

const {Client, CryptoTransferTransaction, mainnetNodes, testnetNodes } = require('@hashgraph/sdk');

h_network = process.argv[2];
h_name = process.argv[3];
operator = { account: process.argv[4], privateKey: process.argv[5] };
recipient_accountId = process.argv[6];
amount = process.argv[7];
memo = process.argv[8];
bashVarName = process.argv[9];

async function main(client) {

    const tx = await new CryptoTransferTransaction()
                    .addSender(operator.account, amount)
                    .addRecipient(recipient_accountId, amount)
                    .setTransactionMemo(memo)
                    .execute(client);

    // const receipt =  await tx.getReceipt(client);
    const receipt = await tx.getReceipt(client);

    console.log( bashVarName + '="' + receipt.status + '"');
    bash.sourcify( receipt, bashVarName );

    process.exit(0)   
}
 
switch(h_network) {
  case "main":
    network = mainnetNodes
    break;
  case "test":
    network = testnetNodes
    break;
  default:
    console.log("Network is not set ", h_network);
}

main(new Client({ network: network, operator: operator }))
  .catch(console.error)
  .then(() => process.exit(1));
