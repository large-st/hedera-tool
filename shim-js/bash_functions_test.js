#!/usr/bin/env node

// import the 'Client' module from the Hedera JS SDK

const bash = require('./bash_functions.js');

bash.sourcify( 0 , 'i' );

bash.sourcify( 0 , 'i' );

bash.sourcify( null , 'i' );

bash.sourcify( { i: 0, j: { k: 2, l: 3} } );

bash.sourcify( { i: 0, j: { k: 2, l: 3} } , 'result' );


bash.sourcify( { i: 0, j: { k: '"a', l: "'b"} } , 'result' );
