#!/usr/bin/env node
// import the 'Mnemonic' module from the Hedera JS SDK

const {Mnemonic}= require('@hashgraph/sdk');

network = process.argv[2];
name = process.argv[3];
// passphrase = process.argv[4];
 
async function main() {

    var secret = await Mnemonic.generate();
    const privateKey = await secret.toPrivateKey( /* passphrase */);
    const key = privateKey.publicKey;
    
    console.log(`${network}_${name}_secretWords='${secret.toString()}'`);
    console.log(`${network}_${name}_secretKey='${privateKey.toString()}'`);
    console.log(`${network}_${name}_publicKey='${key.toString()}'`);
  
    process.exit(0);
}
    
main()
  .catch(console.error)
  .then(() => process.exit(1));
