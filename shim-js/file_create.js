#!/usr/bin/env node
const bash = require('./bash_functions.js');

const {Client, FileCreateTransaction, Ed25519PublicKey, mainnetNodes, testnetNodes } = require('@hashgraph/sdk');

h_network = process.argv[2];
h_name = process.argv[3];
operator = { account: process.argv[4], privateKey: process.argv[5] };
owner_publicKey = Ed25519PublicKey.fromString( process.argv[6] );
maxTbar = process.argv[7];
memo = process.argv[8];
bashVarName = process.argv[9];
contents = process.argv[10];

async function main(client) {
 
  const tx = await new FileCreateTransaction()
                          .addKey( owner_publicKey ) // Defines the "admin" of this file
                          .setTransactionMemo(memo)
                          .setMaxTransactionFee(maxTbar)
                          .setContents( contents )
                          .execute(client);

  const receipt = await tx.getReceipt(client);  
  
  console.log( bashVarName + "='" + receipt.getFileId() + "'" );

  process.exit(0)   
}
 
switch(h_network) {
  case "main":
    network = mainnetNodes
    break;
  case "test":
    network = testnetNodes
    break;
  default:
    console.log("Network is not set ", h_network);
}

main(new Client({ network: network, operator: operator }))
  .catch(console.error)
  .then(() => process.exit(1));
