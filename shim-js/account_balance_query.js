#!/usr/bin/env node

const {Client, AccountBalanceQuery, mainnetNodes, testnetNodes } = require('@hashgraph/sdk');

h_network = process.argv[2];
h_name = process.argv[3];
operator = { account: process.argv[4], privateKey: process.argv[5] };
query_accountId = process.argv[6];

async function main(client) {

   // Attempt to get and display the balance of our account
 
    var currentBalance = await new AccountBalanceQuery()
                                    .setAccountId(query_accountId)
                                    .execute(client);
                                
    console.log(currentBalance);
  
    process.exit(0)   
}
     
switch(h_network) {
  case "main":
    network = mainnetNodes
    break;
  case "test":
    network = testnetNodes
    break;
  default:
    console.log("Network is not set ", h_network);
}

main(new Client({ network: network, operator: operator }))
  .catch(console.error)
  .then(() => process.exit(1));
