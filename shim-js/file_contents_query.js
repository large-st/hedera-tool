#!/usr/bin/env node

const bash = require('./bash_functions.js');

const {Client, FileContentsQuery, mainnetNodes, testnetNodes } = require('@hashgraph/sdk');
const { TextDecoder } = require('util');

h_network = process.argv[2];
h_name = process.argv[3];
operator = { account: process.argv[4], privateKey: process.argv[5] };
query_fileId = process.argv[6];

async function main(client) {

    const contents = await new FileContentsQuery()
                    .setFileId(query_fileId )
                    .execute(client);
                         
    console.log(`${new TextDecoder().decode(contents)}`)

    process.exit(0);  
}
 
switch(h_network) {
  case "main":
    network = mainnetNodes
    break;
  case "test":
    network = testnetNodes
    break;
  default:
    console.log("Network is not set ", h_network);
}

main(new Client({ network: network, operator: operator }))
  .catch(console.error)
  .then(() => process.exit(1));



