#!/usr/bin/env node

const bash = require('./bash_functions.js');

const {Client, FileInfoQuery, mainnetNodes, testnetNodes } = require('@hashgraph/sdk');

h_network = process.argv[2];
h_name = process.argv[3];
operator = { account: process.argv[4], privateKey: process.argv[5] };
query_fileId = process.argv[6];
bashVarName = process.argv[7];

async function main(client) {

    const info = await new FileInfoQuery()
                    .setFileId(query_fileId )
                    .execute(client);
  
    console.log( bashVarName + "='" + query_fileId + "'");
    bash.sourcify( info, bashVarName );

    process.exit(0);  
}
 
switch(h_network) {
  case "main":
    network = mainnetNodes
    break;
  case "test":
    network = testnetNodes
    break;
  default:
    console.log("Network is not set ", h_network);
}

main(new Client({ network: network, operator: operator }))
  .catch(console.error)
  .then(() => process.exit(1));



