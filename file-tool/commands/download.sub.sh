# hedera file download.sub.sh
#
# copywrite Keithy 2020
#

me "$BASH_SOURCE" #tradition

command="download"
description="file download"

options=\
"
$breadcrumbs [ --options ... ] [ account-name | id ... ]
--operator=<name>     who's downloading (default: ${h_whoami})
--confirm | -Y        naturally
"

usage=\
"
$breadcrumbs --id=0.0.12345 mit-licence.txt --id=0.0.12346 apache2.0.txt
$breadcrumbs --tag=mit mit-licence.txt --tag=Ap2 apache2.0.txt
"

$METADATAONLY && return

g_declare_options GET_CONTENTS PRINT_CONTENTS AUTO_MODE #false

PRINT_CONTENTS=true

#for key in "${!file_paths[@]}"

declare -A destination

for arg in "$@"
do
     case "$arg" in
      --node=*)
        HB_NODE="${arg#--node=}"
      ;;
      --op=* | --operator=* )
        HB_OPERATOR="${arg#--op*=}"
      ;;
      --print | -p )
        PRINT_CONTENTS=true
      ;;
      --id=* )
        fileId="${arg#--id=}"
        destination["${fileId}"]="${file_paths['${fileId}']:-}"
        GET_CONTENTS=true
      ;;
      --tag=* )
        tag="${arg#--tag=}"
        fileId=file_ids["$tag"]
        destination["${fileId}"]="${file_paths["$fileId"]:-}"
        GET_CONTENTS=true
      ;;
      -*)
        :
      ;;
      *)
        destination["${fileId}"]="${arg}"
      ;;
    esac
done

source "${h_lib}/common_functions.sh"

h_node_set "${HB_NODE:-}"
h_operator_set "${HB_OPERATOR:-}" "${h_whoami}"

[[ "${#list[@]}" == "0" ]] && { echo "no file ids provided";  exit; }

$CONFIRM || { echo "(append --confirm to query account info for "${list[*]:-}}" via ${h_network}net)" ; exit; }

for query_id in "${!destination[@]}"
do

  if $PRINT_CONTENTS || $GET_CONTENTS; then
    contents=$("${h_shim}/file_contents_query.js" "${h_network}" "${h_node}" "${op_id}" "${op_secretKey}" "${query_id}" || exit 1)
  fi

  if $PRINT_CONTENTS; then
    echo "${contents}" 
  fi

  if $GET_CONTENTS; then
    if [[ -n "${destination[${query_id}]:-}" ]]; then
      echo "${contents}" > "${destination[${query_id}]}"
    fi
  fi

done

exit 0

# License TBD