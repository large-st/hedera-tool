# hedera file upload.sub.sh
#
# copywrite Keithy 2020
#
 
me "$BASH_SOURCE" #tradition

command="file"
description="File Service"

options=\
"
$breadcrumbs [ --options ... ] [ '<memo' ]
--operator=<name>     who's paying  (default: ${h_whoami})
--owner=<name>        named owner   (default: ${h_whoami})
--hbar=<hbar>         maximum amount in hbar (default 1)
--tbar=<tbar>         maximum amount in tinybar
--file <filename>     contents from file
--confirm | -Y        naturally
"

usage=\
"
$breadcrumbs --hbar=1 --to=friend 'Hello Future'
$breadcrumbs --tbar=1 --recipient="0.0.3" 'Hello Future' -V
"

$METADATAONLY && return

g_declare_options FILE_INFO EXPORT_RESULT CONTENTS_FROM_FILE #false

FILE_CREATE=false
memo=""
for arg in "$@"
do
    case "$arg" in
      --node=*)
        HB_NODE="${arg#--node=}"
      ;;
      --op=* | --operator=* )
        HB_OPERATOR="${arg#--op*=}"
      ;;
      --owner=* )
        owner_name="${arg#--owner=}"
      ;;
      --owner-key=* )
        owner_key="${arg#--owner-key=}"
      ;;
      --hbar=* )
        hbar="${arg#--hbar=}"
        (( tbar = hbar * h_onebar ))
      ;;
      --tbar=* )
        tbar="${arg#--tbar=}"
      ;;
      --memo=* )
        memo="${arg#--memo=}"
      ;;
      --id=* )
        file_id="${arg#--id=}"
      ;;
      --create )
        FILE_CREATE=true
      ;;
      --file | -f )
        CONTENTS_FROM_FILE=true
      ;;
      -*)
        :
      ;;
      *)
        contents="$arg"
      ;;
    esac
done

source "${h_lib}/common_functions.sh"

h_node_set "${HB_NODE:-}"
h_operator_set "${HB_OPERATOR:-}" "${h_whoami}"

if [[ -n ${owner_key+x} ]]; then
  owner_name="NA(${owner_key:14:8}...)"
else 
  owner_name="${owner_name:-$op_name}"
  owner_key_var="${h_network}_${owner_name}_publicKey"
  owner_key="${!owner_key_var}"
fi

[[ -z ${tbar+x} ]] && (( tbar = h_onebar )) 
 
$CONFIRM || { echo "(append --confirm to upload file for ${owner_name}(${owner_key:14:8}) memo:'${memo}' max: "${tbar}" via ${h_network}net)" ; exit; }

if $CONTENTS_FROM_FILE; then
  filename="$contents"
  contents=$(cat "$filename")
fi

if $FILE_CREATE; then
    result=$("${h_shim}/file_create.js" "${h_network}" "${h_node}" "${op_id}" "${op_secretKey}" "${owner_key}" "${tbar}" "${memo}" "file_id" "$contents")
    
    $VERBOSE && echo "$result"
    source <(echo "$result")

    echo "$file_id"
fi

$FILE_CREATE || echo "(--create?)"

exit 0

# License TBD