# hedera file info.sub.sh
#
# copywrite Keithy 2020
#

me "$BASH_SOURCE" #tradition

command="info"
description="file information query"

options=\
"
$breadcrumbs [ --options ... ] [ account-name | id ... ]
--operator=<name>     who's asking (default: ${h_whoami})
--confirm | -Y        naturally
"

usage=\
"
$breadcrumbs "0.0.3" "0.0.4" me
$breadcrumbs me bob alice
"

$METADATAONLY && return

g_declare_options GET_INFO #false

GET_INFO=true

list=()
for arg in "$@"
do
    case "$arg" in
      --node=*)
        HB_NODE="${arg#--node=}"
      ;;
      --op=* | --operator=* )
        HB_OPERATOR="${arg#--op*=}"
      ;;
      -*)
        :
      ;;
      *)
        list+=( "$arg" )
      ;;
    esac
done

source "${h_lib}/common_functions.sh"

h_node_set "${HB_NODE:-}"
h_operator_set "${HB_OPERATOR:-}" "${h_whoami}"

[[ "${#list[@]}" == "0" ]] && { echo "no file ids provided";  exit; }

$CONFIRM || { echo "(append --confirm to query account info for "${list[*]:-}}" via ${h_network}net)" ; exit; }

for name in "${list[@]:-${op_name}}"
do
  if [[ "$name" =~ "$h_match_id" ]]; then
      query_id="$name"
  else
      query_id="${file_ids[$name]}"
  fi

  if $GET_INFO; then

    result=$("${h_shim}/file_info_query.js" "${h_network}" "${h_node}" "${op_id}" "${op_secretKey}" "${query_id}" "found" 2> /tmp/hb_file_err || echo "found=''") 
    
    $VERBOSE && echo "$result"
    source <(echo "$result")

    if [[ -z "$found" ]]; then
      $VERBOSE && cat "/tmp/hb_file_err" && rm -f "/tmp/hb_file_err"
      echo "Not found (${query_id})"
    else

      [[ "${found_isDeleted}" == "true" ]] && deletedMsg="Deleted" || deletedMsg="" 
      print_kv "id" "${found_fileId_shard}.${found_fileId_realm}.${found_fileId_file}" "$deletedMsg"
      print_kv "size" "${found_size}"
    fi
  fi

done

exit 0

# License TBD
